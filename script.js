// переменные с элементами
var addBookButton = document.querySelector('.add-book');
var editForm = document.querySelector('.edit-form');
var buttonCancel = document.querySelector('.button-cancel');
var buttonSave = document.querySelector('.button-save');
var buttonDelete = document.querySelectorAll('.delete');
var buttonEdit = document.querySelectorAll('.editing');

var bookName = document.querySelector('.book-header');
var authorName = document.querySelector('.author-name');
var yearRelease = document.querySelector('.year-release');
var bookImage = document.querySelector('.book-image');
var bookContainer = document.querySelector('.books');

// функции-обработчики
function switcher() {
    editForm.classList.toggle('show-edit-form');
    bookContainer.classList.toggle('show-books');

    // очищаем поля формы
    bookName.value = '';
    authorName.value = '';
    yearRelease.value = '';
    bookImage.value = '';
}

function saver(event) {
    // если класс есть, значит редактирование
    if (event.target.classList.contains('button-editor')) {
        var edited = document.querySelector('.edited');
        var image = edited.querySelector('.book-cover');
        var header = edited.querySelector('.book-name');
        var author = edited.querySelector('.authors');
        var yearRel = edited.querySelector('.year-of-release');

        // добавляю введенные пользователем значения
        image.src = bookImage.value;
        header.innerHTML = bookName.value;
        author.innerHTML = authorName.value;
        yearRel.innerHTML = yearRelease.value;
    } else {
        // иначе создание книги
        bookContainer.insertAdjacentHTML('beforeEnd', `<article class="book clearfix"><img src="${bookImage.value}" width="100" height="150" class="book-cover" alt="${bookName.value}"><div class="description"><h3 class="book-name">${bookName.value}</h3><span class="authors">${authorName.value}</span><span class="year-of-release">${yearRelease.value} г.</span></div><div class="buttons"><button class="books-button editing">Редактировать</button><button class="books-button delete">Удалить</button></div></article>`);
    }

    // добавляем заново обработчики событий
    var buttonDelete = document.querySelectorAll('.delete');
    var buttonEdit = document.querySelectorAll('.editing');

    for (var i = 0; i < buttonDelete.length; i = i + 1) {
        buttonDelete[i].addEventListener('click', deleter);
    }

    for (var j = 0; j < buttonEdit.length; j = j + 1) {
        buttonEdit[j].addEventListener('click', editor);
    }

    // скрываем форму, открываем список книг, удаляем класс редактирования книги
    editForm.classList.toggle('show-edit-form');
    bookContainer.classList.toggle('show-books');
    edited.classList.remove('edited');
    buttonSave.classList.remove('button-editor');

    // очищаем поля формы
    bookName.value = '';
    authorName.value = '';
    yearRelease.value = '';
    bookImage.value = '';
}

function editor(event) {
    editForm.classList.toggle('show-edit-form');
    bookContainer.classList.toggle('show-books');
    buttonSave.classList.add('button-editor');

    var book = event.target.parentNode.parentNode;
    book.classList.add('edited');

    // достаю исходные данные книги
    var image = book.querySelector('.book-cover').src;
    var header = book.querySelector('.book-name').textContent;
    var author = book.querySelector('.authors').textContent;
    var yearRel = book.querySelector('.year-of-release').textContent;

    // вывожу полученные значения в input и формы
    bookName.value = header;
    authorName.value = author;
    yearRelease.value = yearRel;
    bookImage.value = image;
}

function deleter(event) {
    var removingBook = event.target.parentNode.parentNode;

    bookContainer.removeChild(removingBook);
}

// добавляем обработчики
addBookButton.addEventListener('click', switcher);

buttonCancel.addEventListener('click', switcher);

buttonSave.addEventListener('click', saver);

for (var i = 0; i < buttonDelete.length; i = i + 1) {
    buttonDelete[i].addEventListener('click', deleter);
}

for (var j = 0; j < buttonEdit.length; j = j + 1) {
    buttonEdit[j].addEventListener('click', editor);
}